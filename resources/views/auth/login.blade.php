@extends('layouts.default')
@section('content')
<div class="container">
    {!! Form::open() !!}
    @include('layouts.messages')
    <div class="form-group">
        {!! Form::label('username', 'Потребителско име') !!}
        {!! Form::text('username', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('password', 'Парола') !!}
        {!! Form::password('password', ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        <label class="control-label">
            {!! Form::checkbox('remember', 1, true)!!} Запомни ме
        </label>
    </div>
    <div class="form-group">
        {!! Form::submit('Вход', ['class' => 'btn btn-primary']) !!}
        <a href="{{ url('auth/register') }}" class="btn btn-default">Регистрация</a>
    </div>
    {!! Form::close() !!}
</div>
@stop
