@extends('layouts.default')
@section('content')
<div class="container">
    {!! Form::open() !!}
    @include('layouts.messages')
    <div class="form-group">
        {!! Form::label('email', 'Имейл') !!}
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
        {!! $errors->first('email') !!}
    </div>
    <div class="form-group">
        {!! Form::label('username', 'Потребителско име') !!}
        {!! Form::text('username', null, ['class' => 'form-control']) !!}
        {!! $errors->first('username') !!}
    </div>
    <div class="form-group">
        {!! Form::label('password', 'Парола') !!}
        {!! Form::password('password', ['class' => 'form-control']) !!}
        {!! $errors->first('password') !!}
    </div>
    <div class="form-group">
        {!! Form::label('repeat_password', 'Повтори паролата') !!}
        {!! Form::password('repeat_password', ['class' => 'form-control']) !!}
        {!! $errors->first('repeat_password') !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Регистрация', ['class' => 'btn btn-primary']) !!}
        <a href="{{ url('auth/login') }}" class="btn btn-default">Вход</a>
    </div>
    {!! Form::close() !!}
</div>
@stop
