@extends('layouts.default')
@section('content')
    <div class="container">
    {!! Form::open(['url' => 'phones'.(isset($item) ? '/'.$item->id : ''), 'method' => (isset($item) ? 'put' : 'post'), 'class' => 'form-horizontal']) !!}
        @include('layouts.messages')
        <div class="form-group">
            {!! Form::label('name', 'Име', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::text('name', $item->name, ['class' => 'form-control']) !!}
                {!! $errors->first('name') !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('phone_number', 'Телефонен номер', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::text('phone_number', $item->phone_number, ['class' => 'form-control']) !!}
                {!! $errors->first('phone_number') !!}
            </div>
        </div>
        <div class="row">
            <div class="col col-sm-10 col-sm-offset-2">
                {!! Form::submit('Запиши', ['class' => 'btn btn-primary']) !!}
                <a href="{{ url('phones') }}" class="btn btn-default">Отказ</a>
            </div>
        </div>
    {!! Form::close() !!}
</div>
@stop
