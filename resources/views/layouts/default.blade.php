<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>{{ $title ? $title.' - ' : '' }}Телефонен указател</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-default">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{{ url() }}">Телефонен указател</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ url() }}">Начало</a></li>
                @if(Auth::guest())
                    <li><a href="{{ url('auth/login') }}">Вход</a></li>
                    <li><a href="{{ url('auth/register') }}">Регистрация</a></li>
                @else
                    <li>
                        <a href="{{ url('auth/logout') }}">Изход</a>
                    </li>
                @endif
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
        @section('header')
            <div class="container">
                <div class="page-header">
                    <h1>{{ $title }}</h1>
                </div>
            </div>
        @show
        @yield('content')
        <footer>
            <p class="text-center">
                &copy; {{ date('Y') }}
                <a href="http://uktcacademy.com">Академия за софтуерна разработка - гр. Правец</a>
            </p>
        </footer>
    </body>
</html>
