@if(session('message'))
    <div class="alert alert-{{ session('message.type') }}">
        @if(session('message.heading'))
            <h4>{{ session('message.heading') }}</h4>
        @endif
        <p>
            {{ session('message.body') }}
        </p>
    </div>
@endif
