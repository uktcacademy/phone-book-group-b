<?php

namespace PhoneBook\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Session;

abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;

    public function __construct() {
		if(Session::has('errors')) {
			Session::get('errors')->getBag('default')->setFormat('<span class="text-block">:message</span>');
		}
	}
}
