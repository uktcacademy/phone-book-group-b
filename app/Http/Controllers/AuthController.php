<?php

namespace PhoneBook\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Auth;
use PhoneBook\User;
use PhoneBook\Http\Requests\Register;

class AuthController extends Controller {
    public function getLogin() {
        return view('auth.login', [
            'title' => 'Вход'
        ]);
    }

    public function postLogin(Request $request) {
        if(Auth::attempt([
            'username' => $request->input('username'),
            'password' => $request->input('password')
        ], $request->input('remember'))) {
            return redirect('/');
        }

        return redirect('auth/login')->with('message', [
            'type' => 'danger',
            'body' => 'Невалидни данни за вход.'
        ])->withInput();
    }

    public function getRegister() {
        return view('auth.register', [
            'title' => 'Регистрация'
        ]);
    }

    public function postRegister(Register $request) {
        $user = User::create([
            'email' => $request->input('email'),
            'username' => $request->input('username'),
            'password' => Hash::make($request->input('password'))
        ]);

        if(!$user) {
            return redirect('auth/register')->with('message', [
                'type' => 'danger',
                'heading' => 'Потребителят не беше създаден.',
                'body' => 'Моля, опитайте по-късно!'
            ]);
        }

        return redirect('auth/login')->with('message', [
            'type' => 'success',
            'heading' => 'Регистрацията е успешна!',
            'body' => 'Вече може да влезете в профила си.'
        ]);
    }

    public function getLogout() {
        Auth::logout();

        return redirect('auth/login')->with('message', [
            'type' => 'success',
            'body' => 'Излязохте от профила си успешно!'
        ]);
    }
}
