<?php

namespace PhoneBook\Http\Controllers;

use Illuminate\Http\Request;

use PhoneBook\Http\Requests;
use PhoneBook\Http\Controllers\Controller;
use PhoneBook\Phone;
use PhoneBook\Http\Requests\SavePhone;

use Auth;

class PhoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('phones.index', [
            'title' => 'Телефонни номера',
            'items' => Phone::where('user_id', Auth::id())
                ->get(['id', 'name', 'phone_number'])
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('phones.save', [
            'title' => 'Добавяне на телефонен номер'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(SavePhone $request)
    {
        $phone = Phone::create([
            'user_id' => Auth::id(),
            'name' => $request->input('name'),
            'phone_number' => $request->input('phone_number')
        ]);

        if(!$phone) {
            return redirect('phones/create')->with('message', [
                'type' => 'danger',
                'body' => 'Телефонният номер не е създаден. Моля, опитайте по-късно!'
            ])->withInput();
        }

        return redirect('phones/create')->with('message', [
            'type' => 'success',
            'body' => 'Телефонният номер е създаден успешно!'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $phone = Phone::where('user_id', Auth::id())->find($id);

        if(!$phone) {
            return redirect('phones')->with('message', [
                'type' => 'danger',
                'body' => 'Телефонният номер не е намерен.'
            ]);
        }

        return view('phones.save', [
            'title' => $phone->name.' - Редактиране на телефонен номер',
            'item' => $phone
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(SavePhone $request, $id)
    {

        $isUpdated = Phone::find($id)->update([
            'name' => $request->input('name'),
            'phone_number' => $request->input('phone_number')
        ]);

        return redirect()->back()->with('message', $isUpdated ? [
            'type' => 'success',
            'body' => 'Телефонният номер е обновен успешно!'
        ] : [
            'type' => 'danger',
            'body' => 'Телефонният номер не е обновен!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $phone = Phone::where('user_id', Auth::id())->find($id);

        if(!$phone) {
            return redirect('phones')->with('message', [
                'type' => 'danger',
                'body' => 'Телефонният номер не е намерен, за да бъде изтрит.'
            ]);
        }

        return redirect('phones')->with('message', $phone->delete() ? [
            'type' => 'success',
            'body' => 'Телефонният номер е изтрит успешно!'
        ] : [
            'type' => 'danger',
            'body' => 'Телефонният номер не е изтрит.'
        ]);
    }
}
