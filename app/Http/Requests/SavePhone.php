<?php

namespace PhoneBook\Http\Requests;

use PhoneBook\Http\Requests\Request as RequestBase;
use Auth;
use PhoneBook\Phone;
use Illuminate\Http\Request;

class SavePhone extends RequestBase
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        $phoneId = $request->segment(2);
        
        if($phoneId && !Phone::where('user_id', Auth::id())->find($phoneId)) {
            return false;
        }
        return !Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:255',
            'phone_number' => 'required|numeric'
        ];
    }
}
