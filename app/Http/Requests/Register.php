<?php

namespace PhoneBook\Http\Requests;

use PhoneBook\Http\Requests\Request;
use Auth;

class Register extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users,email',
            'username' => 'required|min:4|max:100|unique:users,username',
            'password' => 'required|min:6',
            'repeat_password' => 'required|same:password'
        ];
    }
}
